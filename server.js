require('dotenv').config()
const express = require('express');
const cookieParser = require('cookie-parser');
const db = require('./app/models');
// const Role = db.role();
const logger = require('morgan');


const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.use(cookieParser());
app.use(express.static('app/public'));


//set app config

// const title = proccess.env.TITLE;
// const port = proccess.env.PORT;
// const baseUrl = proccess.env.URL + port;
const title = "Hello";
const port = 5000;
const baseUrl = "http://127.0.0.1:"+port;

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Origin',
        'Origin, X-Request-With, Content=Type, Accept, Authorization, x-access-token'
    );
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Method', 'PUT, POST, PATCH, DELETE, GET');
    }
    next();
});

require('./app/router/router')(app);

db.sequelize.sync({
    // force : true
}).then(() => {
    // create_roles();
    app.listen(port, ()=> console.log(title + " run on " + baseUrl))
});

function create_roles(){
    db.Role.create({
        id: 1,
        name: 'USER'
    });

    db.Role.create({
        id:2,
        name: 'ADMIN'
    });

    db.Role.create({
        id: 3,
        name: 'PM'
    });
}