const verifySign = require('../api/verifySign');

const verifySignUpController = require('../api').verifySignUp;
const verifySignController = require('../api').verifySign;
const statusController = require('../api').status;
const verifyJwtController = require('../api').verifyJwtToken;

module.exports = (app) => {
    // User Auth
    app.post('/api/auth/signup', 
            [
                verifySignUpController.checkDuplicateUserNameOrEmail,
                verifySignUpController.checkRolesExisted
            ],
            verifySignController.signup );
    app.post('/api/auth/signin', verifySignController.signin);

    //status
    app.get('/api/status',
            statusController.list
    );

    app.get('/api/statususer', 
            [verifyJwtController.verifyToken],
            statusController.listStatusUser
    );

    app.get('/api/status/:id', 
            [verifyJwtController.verifyToken,
                verifyJwtController.isAdmin     
        ],
        statusController.getById
    );
    app.post('/api/status', 
        [verifyJwtController.verifyToken,
                verifyJwtController.isAdmin
        ],
        statusController.add
    );
    app.put('/api/status/:id',
        [verifyJwtController.verifyToken,
                verifyJwtController.isAdmin
        ],
        statusController.updated
    );

    app.delete('/api/status/:id',
        [verifyJwtController.verifyToken,
                verifyJwtController.isAdmin
        ],
        statusController.delete
    );


   
}